{ config, lib, pkgs, ...}: {

  # mostly based on https://github.com/NixOS/nixpkgs/issues/13901

  # for hplip
  nixpkgs.config.allowUnfree = true;

  hardware.sane = {
    enable = true;
    extraBackends = [ pkgs.hplip ];
  };
  services = {
    printing = {
      enable = true;
      drivers = [ pkgs.hplip ];
      browsing = true;
      defaultShared = true;
      listenAddresses = [ "*:631" ];
      allowFrom = [ "all" ];
    };

    saned = {
      enable = true;
      extraConfig = ''
        10.0.0.0/8
        172.16.0.0/20
        192.168.0.0/16
      '';
    };

    avahi = {
      enable = true;
      nssmdns = true;
      publish = {
        enable = true;
        domain = true;
        userServices = true;
      };
    };
  };

  networking = {
    firewall = {
      allowedTCPPorts = [ 631 ];
      allowedUDPPorts = [ 631 ];
    };
  };
}
