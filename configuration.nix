{ config, pkgs, ... }:

{
  imports = [ 
    <nixos-hardware/pcengines/apu>

    ./hardware-configuration.nix

    ./users.nix
    ./pkgs.nix
    ./print-scan.nix
    ./i18n.nix
    ./eaton.nix
    ./vnstat.nix
    ./clarissa.nix
    ./mqtt.nix
    ./influxdb.nix
    ./grafana.nix
    ./telegraf.nix
    #./nix-mqtt.nix
  ];

  # save some time on rebuilds...
  documentation.enable = false;
  documentation.man.enable = false;

  boot = {
    loader.grub = {
      enable = true;
      device = "/dev/sda";
    };
    kernelPackages = pkgs.linuxPackages_latest;
    # additional optional config for rasdaemon
    #kernelPatches = [
    #  {
    #    name = "memory_failure";
    #    patch = null;
    #    extraConfig = "MEMORY_FAILURE y";
    #  }
    #];
  };

  hardware.rasdaemon.enable = true;

  networking = {
    hostName = "jamshed";
  };

  services = {
    openssh.enable = true;
    tuptime.enable = true;
    fstrim.enable = true;
    iperf3 = {
      enable = true;
      openFirewall = true;
    };
  };

  nix = {
    settings.auto-optimise-store = true;
    gc = {
      automatic = true;
      dates = "Sat *-*-* 03:15:00";
    };
  };

  system.stateVersion = "21.11"; # Did you read the comment?

}

