{ config, lib, pkgs, ...}:

# TODO: find a better name and description

let
  commit = "8cb9400b98eecfa4de40a4c5f7ad66bc7a782615";
  hash = "sha256-b+gYczoa/0SIx7D5kmKqFnQvKlKRDniUUg4AbCZ4wu0=";
  nix-mqtt = pkgs.fetchurl {
    inherit hash;
    url = "https://gitlab.com/evils/nix-mqtt/-/raw/${commit}/nixos-channel-age.sh";
    executable = true;
  };
in
{
  users = {
    groups.nix-mqtt.members = [ "nix-mqtt" ];
    users.nix-mqtt = {
      description = "nix channel mqtt publisher";
      group = "nix-mqtt";
      isSystemUser = true;
    };
  };

  systemd = {
    services.nix-mqtt = {
      description = "nix channel mqtt publisher";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      environment.NIX_PATH = "nixpkgs=${pkgs.path}";
      path = [ pkgs.nix ];
      serviceConfig = {
        Type = "oneshot";
        User = "nix-mqtt";
        ExecStart = "${nix-mqtt} --expire 2000";
      };
    };

    timers.nix-mqtt = {
      description = "nix channel mqtt publishing timer";
      wantedBy = [ "nix-mqtt.service" "timers.target" ];
      partOf = [ "nix-mqtt.service" ];
      timerConfig = {
        Unit = "nix-mqtt.service";
        OnUnitInactiveSec = "420";
        RandomizedDelaySec = "59s";
      };
    };
  };

}
