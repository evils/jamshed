{ config, lib, pkgs, ...}: {

  services.vnstat.enable = true;

  environment.etc."vnstat.conf".text = ''
    MonthRotate 25
  '';

}
