{ config, lib, pkgs, ...}: {

  environment.systemPackages = with pkgs; [
    # system utilities
    bc jq gcc git zsh tree screen
    rsync unzip gnupg btrfs-progs
    pciutils usbutils moreutils
    curl wget dhcp
    # informational
    arp-scan htop smartmontools hddtemp
    pv lm_sensors dmidecode lshw lsof
    cbmem clar
    linuxPackages_latest.cpupower
    # miscellaneous
    vim f3 neofetch speedtest-cli
    direnv iperf3 beep thefuck
    alacritty ripgrep
  ];

  programs = {
    vim.defaultEditor = true;
    gnupg.agent.enable = true;
    usbtop.enable = true;

    zsh = {
      enable = true;
      autosuggestions = {
        enable = true;
        strategy = [ "match_prev_cmd" ];
      };
      enableCompletion = true;
      syntaxHighlighting.enable = true;
      interactiveShellInit = ''
        ZSH_DISABLE_COMPFIX=true
        export ZSH=$HOME/.oh-my-zsh
        source $ZSH/oh-my-zsh.sh
        ZSH_CUSTOM=$ZSH/custom/
        export NIX_PAGER=cat
        export DIRENV_LOG_FORMAT=""
        eval "$(direnv hook zsh)"
      '';
      promptInit = "";
    };
  };

}
