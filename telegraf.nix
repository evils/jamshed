{ config, lib, pkgs, ...}: {

  services.telegraf = {
    enable = true;
    # contains a line like INFLUX_TOKEN=Tml4T1MgcnVsZXMgCg==
    environmentFiles = [ "/home/evils/.config/telegraf.env" ];
    extraConfig = {
      "agent" = {
        "interval" = "10s";
        "round_interval" = true;
        "metric_batch_size" = 1000;
        "metric_buffer_limit" = 10000;
        "collection_jitter" = "0s";
        "flush_interval" = "10s";
        "flush_jitter" = "0s";
        "precision" = "";
        "debug" = false;
        "quiet" = false;
        "logfile" = "";
        "hostname" = "";
        "omit_hostname" = false;
      };
      "outputs" = {
        "influxdb_v2" = [
          {
            "urls" = [ "http://influxdb:8086" ];
            "token" = "$INFLUX_TOKEN";
            "organization" = "home";
            "bucket" = "jamshed";
            "namedrop" = [
              "mqtt_consumer"

              "ping"

              "hsg_integers"
              "hsg_floats"
              "hsg_bools"

              "nix_channels_advanced"
              "nix_channels_commits"
            ];
          }
          {
            "urls" = [ "http://influxdb:8086" ];
            "token" = "$INFLUX_TOKEN";
            "organization" = "home";
            "bucket" = "test";
            "namedrop" = [
              "home_floats"
              "utilities_sensor_floats"
              "utilities_sensor_ints"
              "utilities_sensor_strings"

              "ping"

              "hsg_integers"
              "hsg_floats"
              "hsg_bools"

              "nix_channels_advanced"
              "nix_channels_commits"
            ];
            "namepass" = [ "mqtt_consumer" ];
          }
          {
            "urls" = [ "http://influxdb:8086" ];
            "token" = "$INFLUX_TOKEN";
            "organization" = "home";
            "bucket" = "hsg";
            "namepass" = [
              "ping"

              "hsg_integers"
              "hsg_floats"
              "hsg_bools"
            ];
          }
          {
            "urls" = [ "http://influxdb:8086" ];
            "token" = "$INFLUX_TOKEN";
            "organization" = "home";
            "bucket" = "nix";
            "namepass" = [
              "nix_channels_advanced"
              "nix_channels_commits"
            ];
          }
        ];
      };
      "inputs" = {
        "cpu" = {
          "percpu" = true;
          "totalcpu" = true;
          "collect_cpu_time" = false;
          "report_active" = false;
        };
        "disk" = {
          "ignore_fs" = [
            "tmpfs"
            "devtmpfs"
            "devfs"
            "overlay"
            "aufs"
            "squashfs"
            "SystemPS"
          ];
        };
        "diskio" = {};
        "mem" = {};
        "processes" = {};
        "swap" = {};
        "system" = {};
        "kernel" = {};
        "ras" = {};

        "mqtt_consumer" = [
          {
            "name_override" = "home_floats";
            "servers" = [ "tcp://mqtt:1883" ];
            "topics" = [
              "room/desk/temp"
              "room/plate/temp"
              "room/plate/pres"
            ];
            "data_format" = "value";
            "data_type" = "float";
            "tagexclude" = [ "host" ];
          }
          {
            "name_override" = "utilities_sensor_floats";
            "servers" = [ "tcp://mqtt:1883" ];
            "topics" = [
              "home/utilities/sensor/energy_day/state"
              "home/utilities/sensor/energy_night/state"
              "home/utilities/sensor/power/state"
              "home/utilities/sensor/voltage_l1/state"
              "home/utilities/sensor/current_l1/state"
              "home/utilities/sensor/power_l1/state"
              "home/utilities/sensor/gas/state"
            ];
            "data_format" = "value";
            "data_type" = "float";
            "tagexclude" = [ "host" ];
          }
          {
            "name_override" = "utilities_sensor_ints";
            "servers" = [ "tcp://mqtt:1883" ];
            "topics" = [
              "home/utilities/sensor/tariff/state"
            ];
            "data_format" = "value";
            "data_type" = "float";
            "tagexclude" = [ "host" ];
          }
          {
            "name_override" = "utilities_sensor_strings";
            "servers" = [ "tcp://mqtt:1883" ];
            "topics" = [
              "home/utilities/sensor/time/state"
            ];
            "data_format" = "value";
            "data_type" = "string";
            "tagexclude" = [ "host" ];
          }
          {
            "name_override" = "nix_channels_advanced";
            "servers" = [ "tcp://nix.evils.eu:1883" ];
            "topics" = [ "nix/channels/+/advanced" ];
            "data_format" = "value";
            "data_type" = "integer";
            "tagexclude" = [ "host" ];
          }
          {
            "name_override" = "nix_channels_commits";
            "servers" = [ "tcp://nix.evils.eu:1883" ];
            "topics" = [ "nix/channels/+/commit" ];
            "data_format" = "value";
            "data_type" = "string";
            "tagexclude" = [ "host" ];
          }

          {
            "name_override" = "hsg_integers";
            "servers" = [ "$HSG_MQTT" ];
            "topics" = [
              "hsg/clarissa/identified"
              "hsg/clarissa/unidentified"

              "hsg/aio/sensor/mh-z19_co2_value/state"
              "hsg/aio/sensor/mh-z19_temperature/state"
              "hsg/aio/sensor/particulate_matter_10m_concentration/state"
              "hsg/aio/sensor/particulate_matter_25m_concentration/state"
              "hsg/aio/sensor/particulate_matter_100m_concentration/state"
            ];
            "data_format" = "value";
            "data_type" = "integer";
            "tagexclude" = [ "host" ];
          }
          {
            "name_override" = "hsg_bools";
            "servers" = [ "$HSG_MQTT" ];
            "topics" = [
              "hsg/doorkeeper/lock/status"
            ];
            "data_format" = "value";
            "data_type" = "boolean";
            "tagexclude" = [ "host" ];
          }
          {
            "name_override" = "hsg_floats";
            "servers" = [ "$HSG_MQTT" ];
            "topics" = [
              "hsg/heater/temperature"

              "hsg/aio/sensor/ds18b20/state"
            ];
            "data_format" = "value";
            "data_type" = "float";
            "tagexclude" = [ "host" ];
          }
        ];
        "ping" = {
          "urls" = [
            "hackerspace.gent"
            "evils.eu"
            "nix.evils.eu"
            "1.1"
          ];
          "method" = "native";
          #"binary" = "${pkgs.iputils}/bin/ping";
          "tagexclude" = [ "host" ];
        };
      };
    };
  };

  systemd.services.telegraf.path = [ pkgs.lm_sensors ];
  services.telegraf.extraConfig."inputs"."sensors" = {};
}
