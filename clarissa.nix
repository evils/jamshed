{ config, lib, pkgs, ...}: {

  services.clarissa.enable = true;

  environment.systemPackages = with pkgs; [
    clarissa
  ];

}
