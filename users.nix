{ config, lib, pkgs, ...}: {

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.evils = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = [ "wheel" "dialout" "lp" "scanner" "beep" "ups" ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBj6N5zlcXIg5RzmTFovzGU3a80LvXUmnkBbIT29HuoS evils@valix"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBYYndbZfPK2b89ujR7Muzxv6YXo/U+mDkFdtR73p88H evils@evils-nix"
    ];
    initialHashedPassword = "$6$PBYCyjsto4m$2Gg4jckLC6gLpGBC0LrND0HrtASkqTi4q79yN9dM5KFFaoYLquKpmf97WVtIJ0e1pZSw3QJlUO1ZPHfGJL4cE0";
  };

}
