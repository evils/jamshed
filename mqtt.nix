{ config, lib, pkgs, ...}: {

  services.mosquitto = {
    enable = true;
    listeners = [ {
      acl = [ "pattern readwrite #" "pattern read $SYS/#" ];
      settings.allow_anonymous = true;
    } ];
  };

  networking = {
    firewall = {
      allowedTCPPorts = [
        1883
        # 8883
      ];
    };
  };

  environment.systemPackages = with pkgs; [
    mosquitto
  ];

}
