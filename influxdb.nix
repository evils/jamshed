{ config, lib, pkgs, ...}: {

  services.influxdb2 = {
    enable = true;
    settings = {
      "reporting-disabled" = true;
    };
  };

  networking = {
    firewall = {
      allowedTCPPorts = [
        8086
      ];
    };
  };

  environment.systemPackages = with pkgs; [
    influxdb2
  ];

}
