{ config, lib, pkgs, ...}: {

  boot.initrd.kernelModules = [ pcspkr ];

  users.groups = {
    beep = { };
  };

  services.udev = {
    extraRules = ''
      # beep
      ACTION=="add", SUBSYSTEM=="input", ATTRS{name}=="PC Speaker", ENV{DEVNAME}!="", GROUP="beep", MODE="0620"
    '';
  };

}
