{ config, lib, pkgs, ...}: {

  power.ups = {
    enable = false;
    ups.eaton = {
      description = "eaton ellipse pro 1600 iec";
      summary = "eaton via hsg";
      driver = "usbhid-ups";
      port = "auto";
    };
  };

  # VID/PID from nut-scanner
  services.udev.extraRules = ''
    SUBSYSTEM=="hidraw", SUBSYSTEMS=="usb", ATTRS{idVendor}=="0463", ATTRS{idProduct}=="FFFF", GROUP="ups", MODE="0660"
  '';

}
